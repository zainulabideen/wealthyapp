//
//  NetworkManager.swift
//  Multivendor
//
//  Created by Zainul on 10/02/21.
//

import Foundation
import Alamofire

class NetworkingClient {
    func getRequest(urlstring:String, onCompletion: @escaping (Data?,Error?) -> Void) {
        print("API URL : ", urlstring)
        let url = URL.init(string: urlstring)
        let request = URLRequest.init(url: url!)
        AF.request(request).response { (response) in
            print(response.result)
            DispatchQueue.main.async {
                switch response.result {
                case .success( _):
                    let data = response.data
                    onCompletion(data, nil)
                case .failure(let encodingError):
                    print(encodingError)
                    onCompletion(nil,encodingError)
                    break
                }
            }
        }
    }
}
