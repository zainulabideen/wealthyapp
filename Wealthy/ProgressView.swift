//
//  ProgressView.swift
//  Wealthy
//
//  Created by Mac User on 26/08/21.
//

import Foundation
import UIKit
class ProgressView: UIViewController {
 
    static let shared = ProgressView()
    
    var activityIndicatorView = UIActivityIndicatorView(style: .white)
    
    func showLoading() {
        if let topVC = UIApplication.topViewController() {
            let loadingView = UIView(frame: topVC.view.bounds)
            loadingView.tag = 999
            loadingView.accessibilityIdentifier = "loading_view"
            loadingView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            let activityIndicatorView = UIActivityIndicatorView(style: .white)
            activityIndicatorView.center = loadingView.center
            activityIndicatorView.color = UIColor(displayP3Red: 48/255, green: 31/255, blue: 101/255, alpha: 1)
            activityIndicatorView.startAnimating()
            loadingView.addSubview(activityIndicatorView)
            topVC.view.insertSubview(loadingView, at: 999)
        }
    }
    
    func removeLoading() {
        if let topVC = UIApplication.topViewController() {
            if let loadingView = topVC.view.subviews.first(where: { $0.tag == 999 && $0.accessibilityIdentifier == "loading_view" }) {
                loadingView.removeFromSuperview()
            }
        }
    }
}
