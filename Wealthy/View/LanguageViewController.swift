//
//  LanguageViewControllerTableViewController.swift
//  Wealthy
//
//  Created by Zainul on 26/08/21.
//

import UIKit
protocol LanguageDelegate {
    func setLanguage(lang:String)
}
class LanguageViewController: UITableViewController {
    let languages = ["English","Hindi"]
    var delegate: LanguageDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return languages.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = languages[indexPath.row]

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        setLanguage(indexPath.row)
    }

    func setLanguage(_ index:Int) {
        if index == 0 {
        UserDefaults.standard.setValue("en", forKey: "lang")
        } else {
            UserDefaults.standard.setValue("hi", forKey: "lang")
        }
        delegate?.setLanguage(lang: languages[index])
        navigationController?.popViewController(animated: true)
    }

}
