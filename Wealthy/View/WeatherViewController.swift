//
//  ViewController.swift
//  Wealthy
//
//  Created by Zainul on 26/08/2021.
//  Copyright © 2019 App Brewery. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherViewController: UIViewController {
    @IBOutlet weak var langButton: UIButton!
    @IBOutlet weak var conditionImageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    
    var weatherViewModel = WeatherViewModel()
    let locationManager = CLLocationManager()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Navigation Bar:
        navigationController?.navigationBar.barTintColor = UIColor(named: "weatherColour")

        // Navigation Bar Text:
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]

        // Tab Bar:
        tabBarController?.tabBar.barTintColor = UIColor.black

        // Tab Bar Text:
        tabBarController?.tabBar.tintColor = UIColor.black
    }
    fileprivate func setLanguage() {
        if let langSet = UserDefaults.standard.value(forKey: "lang") as? String {
            langButton.setTitle("English >".localizableString(loc:langSet), for: .normal)
            self.title = "Weather".localizableString(loc: langSet)
            self.searchTextField.placeholder = "Search".localizableString(loc: langSet)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLanguage()
        langButton.setTitleColor(UIColor.black, for: .normal)
        langButton.addTarget(self, action: #selector(navigateToLang), for: .touchUpInside)
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        weatherViewModel.delegate = self
        searchTextField.delegate = self
    }
    
    @objc func navigateToLang() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let langVC = storyboard.instantiateViewController(withIdentifier: "LangVC") as! LanguageViewController
        langVC.delegate = self
        self.navigationController?.pushViewController(langVC, animated: true)
    }
}

//MARK:-Language Delegate
extension WeatherViewController:LanguageDelegate {
    func setLanguage(lang: String) {
        self.langButton.setTitle(lang, for: .normal)
        setLanguage()
    }
    
    
}
//MARK: - UITextFieldDelegate

extension WeatherViewController: UITextFieldDelegate {
    
    @IBAction func searchPressed(_ sender: UIButton) {
        searchTextField.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.endEditing(true)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            return true
        } else {
            textField.placeholder = "Type something"
            return false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let city = searchTextField.text {
            weatherViewModel.fetchWeather(cityName: city)
        }
        
        searchTextField.text = ""
        
    }
}

//MARK: - WeatherManagerDelegate


extension WeatherViewController: WeatherManagerDelegate {
    
    func didUpdateWeather(_ weatherManager: WeatherViewModel, weather: WeatherModel) {
        DispatchQueue.main.async {
            self.temperatureLabel.text = weather.temperatureString
            self.conditionImageView.image = UIImage(systemName: weather.conditionName)
            self.cityLabel.text = weather.cityName
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
}

//MARK: - CLLocationManagerDelegate


extension WeatherViewController: CLLocationManagerDelegate {
    
    @IBAction func locationPressed(_ sender: UIButton) {
        DispatchQueue.main.async {
            ProgressView.shared.showLoading()
        }
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            locationManager.stopUpdatingLocation()
            let lat = location.coordinate.latitude
            let lon = location.coordinate.longitude
            weatherViewModel.fetchWeather(latitude: lat, longitude: lon)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}


